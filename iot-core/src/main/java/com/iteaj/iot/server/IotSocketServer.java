package com.iteaj.iot.server;

import com.iteaj.iot.LifeCycle;
import com.iteaj.iot.PortType;
import com.iteaj.iot.config.ConnectProperties;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 服务端套接字
 */
public interface IotSocketServer extends LifeCycle {

    /**
     * 服务端配置
     * @see ConnectProperties#getPort()
     * @see ConnectProperties#getHost()
     * @return
     */
    ConnectProperties config();

    /**
     * 返回端口类型
     * @return
     */
    PortType getPortType();

    /**
     * 关闭socket服务
     */
    @Override
    void close();

    /**
     * 返回设备解码器
     * @return
     */
    ChannelInboundHandlerAdapter getMessageDecoder();

    Logger LOGGER = LoggerFactory.getLogger(IotSocketServer.class);
}
