package com.iteaj.iot.test.client.udp;

import com.iteaj.iot.client.udp.UdpClientMessage;
import com.iteaj.iot.message.DefaultMessageHead;

public class UdpClientTestMessage extends UdpClientMessage {

    public UdpClientTestMessage(byte[] message) {
        super(message);
    }

    public UdpClientTestMessage(MessageHead head) {
        super(head);
    }

    @Override
    protected MessageHead doBuild(byte[] message) {
        return new DefaultMessageHead(message);
    }
}
