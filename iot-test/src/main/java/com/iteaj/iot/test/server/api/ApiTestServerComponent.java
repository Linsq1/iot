package com.iteaj.iot.test.server.api;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.component.DelimiterBasedFrameDecoderServerComponent;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import io.netty.buffer.Unpooled;

public class ApiTestServerComponent extends DelimiterBasedFrameDecoderServerComponent<ApiTestServerMessage> {

    public ApiTestServerComponent(ConnectProperties connectProperties) {
        super(connectProperties, 256, Unpooled.wrappedBuffer("\n".getBytes()));
    }

    @Override
    public String getDesc() {
        return "用于做[FrameworkManager]Api测试";
    }

    @Override
    protected ClientInitiativeProtocol<ApiTestServerMessage> doGetProtocol(ApiTestServerMessage message, ProtocolType type) {
        if(type == ApiProtocolType.Stop) {
            return new ApiServerStopTestProtocol(message);
        } else if(type == ApiProtocolType.Close) {
            return new ApiServerCloseTestProtocol(message);
        }

        return null;
    }

    @Override
    public String getName() {
        return "FSS Api Test";
    }
}
